"use strict";

let speed = [3, 4, 3];
let beg = [6, 4, 10, 5, 5, 10, 20];
let dead = new Date(2019, 10, 24);

const countHoursForTask = (speedTeam, tasks) => {
    let daySpeedOfTeam = 0;
    let pointsForBegLog = 0;
    speedTeam.forEach(item => daySpeedOfTeam += item);
    tasks.forEach(item => pointsForBegLog += item);

    return (pointsForBegLog / daySpeedOfTeam);
};


const evaluationOfWorkOfTeamDev = (speedWorkTeam, begLog, dateDeadline) => {
    console.log(countHoursForTask(speedWorkTeam, begLog));
    let daysWork = 0;
    let todayDay = new Date();

    console.log(todayDay);
};

console.log(evaluationOfWorkOfTeamDev(speed, beg, dead));

function date(numberOfDaysInMonth) {
    const a = numberOfDaysInMonth.getDate() - new Date().getDate;
    const currentDate = new Date();
    const currentDay = currentDate.getDate();
    const enterDate = currentDate.setDate(currentDate + numberOfDaysInMonth);
    const neededDate = new Date(enterDate);

    return a;
}

console.log(date(dead));