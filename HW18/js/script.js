// Creating object student

const student = {
    name: null,
    'last name': null,
    createTabel() {
        return this.tabel = {
            setSubjectandMark () {
                let subject,
                    mark;
                do {
                    subject = prompt('Please, enter a subject here', '');
                    while (subject === '') {
                        subject = prompt('You have entered a wrong subject here, please try again', '');
                    }
                    
                    if (subject === null) {
                        break 
                    } else {
                        mark = +prompt('Please enter here your mark for', +subject, '');
                        while (mark === '' || mark == null || isNaN(mark)) {
                            mark = +prompt('You have entered a wrong mark, or didn`t enter a mark. Please, try again', '');
                        }
                        student.tabel[subject] = mark;
                    }
                } while (subject != null);
            },
        }
    },

    countBadMarks() {
        let countBadMarks = 0;
        for (let i in this.tabel) {
            if (this.tabel[i] < 4) {
                countBadMarks++
            }
        }
        if (countBadMarks === 0) {
            alert('Student has been transfered forward!')
        } else {
            alert(`Student have bad marks: ${countBadMarks}.`);
        }
    },

    averageMarks() {
        let sumMark = 0,
            countMarks = 0;
        for (let i in this.tabel) {
            sumMark += this.tabel[i];
            countMarks++;
        }
        if (sumMark / countMarks > 7) {
            alert('Student have achieved scholarship!');
        } else {
            alert('Student didn`t achieved scholarship! Study better!');
        }
    }
};

// Function which asks First Name and Last Name

const setName = () => {
    let firstName = prompt('Please enter here your first name', '');
    while (firstName === '' || firstName == null) {
        firstName = prompt('You didn`t entered your first name. Please enter your first name again', '');
    }
    return firstName
};

const setLastName = () => {
    let lastName = prompt('Please enter here your last name', '');
    while (lastName === '' || lastName == null) {
        lastName = prompt('You didn`t entered your last name. Please enter your last name again', '');
    }
    return lastName
};

// Set student`s first and last names

student.name = setName();
student['lastName'] = setLastName();

// Calling methods 

student.createTabel().setSubjectandMark();
student.countBadMarks();
student.averageMarks();