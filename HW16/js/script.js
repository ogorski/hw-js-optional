let userNumber = +prompt("Please, enter a number right here", '');
while (userNumber === '' || isNaN(userNumber) || userNumber == null || (userNumber % 1 !==0)) {
    userNumber = +prompt('You entered a wrong number, please try again', userNumber);
}

const getFactorial = (n) => {
    return (n !== 1) ? n * getFactorial(n - 1) : 1;
};

alert(getFactorial(userNumber));