let exmapleObj = {
    "watch": ["cell", 1337, 2142],
    "spectate": {
        1337501: "hacking",
        "independence": 361,
        "freedom": {
            132521: "group",
            2511516: "coup" 
        }
    },
    "price": "hangover"
};

function objectClone(obj) {
    return JSON.parse(JSON.stringify(obj));
}

console.log(objectClone(exmapleObj));